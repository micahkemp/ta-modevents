
# encoding = utf-8

import os
import sys
import time
import datetime
import random
from mako.template import Template

import splunklib.client as client
import splunklib.results as results

def validate_input(helper, definition):
    """Implement your own validation logic to validate the input stanza configurations"""
    # This example accesses the modular input variable
    # lookup = definition.parameters.get('lookup', None)
    # event_format = definition.parameters.get('event_format', None)
    # name_field = definition.parameters.get('name_field', None)
    # value_field = definition.parameters.get('value_field', None)
    # weight_field = definition.parameters.get('weight_field', None)
    # max_duration = definition.parameters.get('max_duration', None)
    # max_count = definition.parameters.get('max_count', None)
    # min_delay = definition.parameters.get('min_delay', None)
    # max_delay = definition.parameters.get('max_delay', None)
    pass

def collect_events(helper, ew):
    # The following examples get the arguments of this input.
    # Note, for single instance mod input, args will be returned as a dict.
    # For multi instance mod input, args will be returned as a single value.
    opt_lookup = helper.get_arg('lookup')
    opt_event_format = helper.get_arg('event_format')
    opt_name_field = helper.get_arg('name_field')
    opt_value_field = helper.get_arg('value_field')
    opt_weight_field = helper.get_arg('weight_field')
    opt_max_duration = float(helper.get_arg('max_duration'))
    opt_max_count = int(helper.get_arg('max_count'))
    opt_min_delay = float(helper.get_arg('min_delay'))
    opt_max_delay = float(helper.get_arg('max_delay'))

    all_values_for_field = {}

    lookup_search = "| inputlookup {}".format(opt_lookup)
    # XXX is helper.context_meta undocumented
    session_key = helper.context_meta['session_key']
    service = client.connect(token=session_key)
    job = service.jobs.create(lookup_search, exec_mode="blocking")
    for result in results.ResultsReader(job.results()):
        all_values_for_field.setdefault(result[opt_name_field], [])
        for index in range(0, int(result[opt_weight_field])):
            all_values_for_field[result[opt_name_field]].append(result[opt_value_field])

    count = 0
    elapsed_duration = 0

    start_time = time.time()

    while count<opt_max_count and elapsed_duration<opt_max_duration:
        event_value_for_field = {}
        for field in all_values_for_field.keys():
            max_index = len(all_values_for_field[field]) - 1
            event_value_index = random.randint(0, max_index)
            event_value_for_field[field] = all_values_for_field[field][event_value_index]

        event_raw = Template(opt_event_format).render(**event_value_for_field)
        event = helper.new_event(event_raw, time=start_time+elapsed_duration, host=None, index=None, source=None, sourcetype=None, done=True, unbroken=True)
        ew.write_event(event)

        count += 1

        random_duration = random.uniform(opt_min_delay, opt_max_delay)
        elapsed_duration += random_duration
